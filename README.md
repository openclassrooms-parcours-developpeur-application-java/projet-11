# PROJET 11

**PROJET 11 - CONSTRUISEZ UNE VEILLE TECHNOLOGIQUE**

**DESCRIPTION**

Un développeur doit rester constamment à l'affût des nouveautés technologiques, qui évoluent en continu. La veille technologique est pour cela une étape indispensable du quotidien.

Les bons développeurs suivent régulièrement les nouveautés du secteur (tous les jours et non pas toutes les semaines). Pour rester à jour, vous allez dès maintenant construire votre outil de veille technologique :

    1. Listez les technologies / thématiques que vous souhaitez suivre
    2. Listez les sites d'actualité qui peuvent vous informer sur ces technologies et thématiques
    3. Construisez une page de tableau de bord qui vous servira de veille (par exemple un tableau de bord sur Netvibes) avec les dernières actualités de ces sites
    4. Créez un compte Twitter (si vous n'en avez pas déjà un), cherchez et suivez-y plusieurs influenceurs du développement web

**LIVRABLES ATTENDUS**

    • Tableau de bord de veille technologique
    • Fil Twitter avec une liste d'influenceurs pertinents 
